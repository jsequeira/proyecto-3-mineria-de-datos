# librerias
install.packages("ggplot2")#------>PARA GRAFICOS
install.packages("dplyr")#-------->OTRAS FUNCIONES
install.packages("psych")#-------->GRAFICO PAIR.PANELS
install.packages("xlsx")#-------->GRAFICO PAIR.PANELS
library(ggplot2)
library(dplyr,warn.conflicts = FALSE)
library(corrplot)
library(GGally)
library(PerformanceAnalytics)
library(psych)
#SE CARGA EL ARCHIVO

datasource <- read.csv("datasource.csv",sep=";",header=T)

#SE CREA LA VISTA

View(datasource)

# CONOCIENDO LOS TIPO DE DATOS

glimpse(datasource)

summary(datasource)

class(datasource$nombre_compania)

str(datasource)

head(datasource)

tail(datasource)

colnames(datasource)

names(datasource)

#SE CREA LA VISTA DE DATOS NULOS

Datosnulos=data.frame(print(apply(is.na(datasource),2, mean)))

View(Datosnulos)

attach(datasource)

NCANTIDAD=as.double(cantidad)
NPRECIO=as.double(precio_unidad)
NANNIO=as.integer(annio)
NMES=as.integer(mes)
NDIA=as.integer(dia)
NFECHA=as.Date(fecha,format = "%M/%d/%Y")

TOTAL=NCANTIDAD*NPRECIO


NDATASOURCE=data.frame(NCANTIDAD,NPRECIO,TOTAL,NANNIO,NMES,NDIA)
NDATASOURCE1=data.frame(NCANTIDAD,NPRECIO,TOTAL,NANNIO,NMES,NDIA,NFECHA)

View((NDATASOURCE))

cor(NDATASOURCE)

correlacion<-round(cor(NDATASOURCE),2)

par(mfrow=c(1,1))
corrplot (correlacion, method = "number", type = "upper")
chart.Correlation(correlacion, histogram=F, pch=5)
pairs(correlacion)
pairs.panels(correlacion)

plot(NDATASOURCE$TOTAL~NDATASOURCE$NCANTIDAD,data=NDATASOURCE)
abline(lm(NDATASOURCE$TOTAL~NDATASOURCE$NCANTIDAD,data=NDATASOURCE),col="blue")

plot(NDATASOURCE$TOTAL~NDATASOURCE$NPRECIO,data=NDATASOURCE)
abline(lm(NDATASOURCE$TOTAL~NDATASOURCE$NPRECIO,data=NDATASOURCE),col="blue")

plot(NDATASOURCE$TOTAL~NDATASOURCE$NANNIO,data=NDATASOURCE)
abline(lm(NDATASOURCE$TOTAL~NDATASOURCE$NANNIO,data=NDATASOURCE),col="blue")

cor.test(~NDATASOURCE$NANNIO+NDATASOURCE$TOTAL,data=NDATASOURCE, method="pearson", conf.level=0.95 )
cor.test(~NDATASOURCE$NANNIO+NDATASOURCE$TOTAL,data=NDATASOURCE, method="spearman", conf.level=0.95 )

par(mfrow=c(2,3))
for (i in 1:5) {
  boxplot(NDATASOURCE[,i],main=names(NDATASOURCE)[i],col="blue",medcol="red",pch=23,cex=1.5,whiskcol="red")
  
}


